package com.example.misviajes;

import android.app.Activity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;

public class TriposoInfoWindowAdapter implements GoogleMap.InfoWindowAdapter{
    private final Activity activity;

    public TriposoInfoWindowAdapter(Activity activity) {
        this.activity = activity;
    }

    @Nullable
    @Override
    public View getInfoWindow(@NonNull Marker marker) {
        return null;
    }

    @Nullable
    @Override
    public View getInfoContents(@NonNull Marker marker) {
        View view = activity.getLayoutInflater()
                .inflate(R.layout.info_window, null);

        Triposo triposo = (Triposo) marker.getTag();
        ImageView img = view.findViewById(R.id.iv_foto);
        TextView tvlocation = view.findViewById(R.id.tv_location_id_mapa);
        TextView tvname = view.findViewById(R.id.tv_name_map);
        if(triposo!=null) {
            tvlocation.setText(triposo.getLocation_id());
            tvname.setText(triposo.getName());
            Glide.with(activity).load(triposo.getImage()).into(img);
        }
        else{
            tvlocation.setText("Posicion tiempo real");
            tvname.setText("empezar mision");
            Glide.with(activity).load(R.drawable.ic_4043232_avatar_batman_comics_hero_113278).into(img);
        }




        return view;
    }
}
