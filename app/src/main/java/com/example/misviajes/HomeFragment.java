package com.example.misviajes;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import java.util.ArrayList;

public class HomeFragment extends Fragment {

    private TriposoAdapter adapter;
    String ciudad;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);

        ListView listView = view.findViewById(R.id.listViewMenu);

        ArrayList<Triposo> items = new ArrayList<>();

        adapter=new TriposoAdapter(
                getContext(),
                R.layout.lv_cities_row,
                items
        );
        listView.setAdapter(adapter);

        /**
         * SPINNER
         */
        try {
            Spinner spinner = view.findViewById(R.id.spinner);
            // Create an ArrayAdapter using the string array and a default spinner layout
            ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(
                    getContext(),
                    R.array.city_array,
                    android.R.layout.simple_spinner_item
            );
            // Specify the layout to use when the list of choices appears
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            // Apply the adapter to the spinner
            spinner.setAdapter(adapter);

            //Sirve para saber que ciudad he selecionado
            spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                    ciudad = parent.getItemAtPosition(pos).toString();
                    System.out.println("1 - ciudad selecionada: " + ciudad);
                    //Aqui se carga el contenido porque llama a la TriposoAPI
                    refresh();
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });

        } catch (Exception e) {
            System.out.println("Errore: " + e);
        }

        /**
         * ABRIR LOS DETALLES DE EL POI SELECIONADO
         */
        listView.setOnItemClickListener((adapterView, view1, i, l) -> {
            Triposo triposo = (Triposo) adapterView.getItemAtPosition(i);
            Intent intent = new Intent(getContext(), DetailActivity.class);
            intent.putExtra("triposo", triposo);
            startActivity(intent);
        });


        return view;
    }

    private class RefreshDataTask extends AsyncTask<Void, Void, ArrayList<Triposo>> {
        @Override
        protected ArrayList<Triposo> doInBackground(Void... voids) {
            TriposoAPI api = new TriposoAPI();
            return api.getPOILocation(ciudad);
        }

        @Override
        protected void onPostExecute(ArrayList<Triposo> triposo) {
            adapter.clear();
            for (Triposo location : triposo) {
                adapter.add(location);
            }
        }
    }

    private void refresh() {
        RefreshDataTask task = new RefreshDataTask();
        task.execute();
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        /*MainViewModel mViewModel = ViewModelProviders.of(this).get(MainViewModel.class);*/
        // TODO: Use the ViewModel
    }




}
