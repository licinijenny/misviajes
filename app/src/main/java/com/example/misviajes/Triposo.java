package com.example.misviajes;

import java.io.Serializable;

public class Triposo implements Serializable {

    private String location_id;
    private String snippet;
    private String name;
    private String image;
    private double latitude;
    private double longitude;
    private String lista;

    public String getLista() {
        return lista;
    }

    public void setLista(String visitada) {
        this.lista = visitada;
    }

    public String getSnippet() {
        return snippet;
    }

    public void setSnippet(String snippet) {
        this.snippet = snippet;
    }

    public String getLocation_id() {
        return location_id;
    }

    public void setLocation_id(String location_id) {
        this.location_id = location_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    @Override
    public String toString() {
        return "Triposo{" +
                "location_id='" + location_id + '\'' +
                ", snippet='" + snippet + '\'' +
                ", name='" + name + '\'' +
                ", image='" + image + '\'' +
                ", latitude=" + latitude +
                ", longitude=" + longitude +
                '}';
    }

}
