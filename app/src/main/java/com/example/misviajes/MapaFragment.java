package com.example.misviajes;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.security.acl.LastOwnerException;

public class MapaFragment extends Fragment implements OnMapReadyCallback, LocationListener {

    private GoogleMap mMap;
    private DatabaseReference reference;
    private DatabaseReference triposo;

    private LocationManager manager;
    //Context mContext;

    private final int MIN_TIME = 1000; // 1 sec
    private final int MIN_DISTANCE = 1; // 1 meter

    Marker mymarker;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_mapa, container, false);

        manager = (LocationManager) getContext().getSystemService(Context.LOCATION_SERVICE);


        FirebaseAuth auth = FirebaseAuth.getInstance();
        reference = FirebaseDatabase.getInstance("https://misviajes-14dd1-default-rtdb.firebaseio.com/").getReference().child("users").child(auth.getUid()).child("currentLocation");
        triposo = FirebaseDatabase.getInstance("https://misviajes-14dd1-default-rtdb.firebaseio.com/").getReference().child("users").child(auth.getUid()).child("misviajes");


        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager()
                .findFragmentById(R.id.map);





        assert mapFragment != null;
        mapFragment.getMapAsync(this::onMapReady);
        
        getLocationUpdate();
        
        readChanges();

        return view;
    }

    private void readChanges() {
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if(snapshot.exists()){
                    try {
                        MyLocation location = snapshot.getValue(MyLocation.class);
                        if (location != null) {
                            mymarker.setPosition(new LatLng(location.getLatitude(), location.getLongitude()));
                        }
                    }catch (Exception e){
                        Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    private void getLocationUpdate() {
        if (manager !=  null){
            if(ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
            ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED){
                if(manager.isProviderEnabled(LocationManager.GPS_PROVIDER)){
                    manager.requestLocationUpdates(LocationManager.GPS_PROVIDER, MIN_TIME, MIN_DISTANCE, this);
                }else if(manager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)){
                    manager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, MIN_TIME, MIN_DISTANCE, this);
                }else{
                    Toast.makeText(getContext(), "No provider Enabled", Toast.LENGTH_SHORT).show();
                }
            }else{
                ActivityCompat.requestPermissions(getActivity(),  new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 101 );
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(requestCode == 101){
            if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                getLocationUpdate();
            }else{
                Toast.makeText(getContext(), "Permission required", Toast.LENGTH_SHORT).show();
            }
        }
    }

    public void onMapReady(GoogleMap googleMap){
        mMap = googleMap;

        //marke para la posiscion entiempo real
        LatLng x = new LatLng(0, 0);
        mymarker = mMap.addMarker(new MarkerOptions()
                .position(x)
                .title("Current position"));
        mMap.getUiSettings().setZoomControlsEnabled(true);
        mMap.getUiSettings().setAllGesturesEnabled(true);
        mMap.moveCamera(CameraUpdateFactory.newLatLng(x));



        //cargo los puntos de la bbddd
        triposo.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                Triposo triposo = dataSnapshot.getValue(Triposo.class);
                TriposoInfoWindowAdapter customInfoWindow = new TriposoInfoWindowAdapter(
                        getActivity()
                );

                assert triposo != null;
                LatLng aux = new LatLng(
                        triposo.getLatitude(),
                        triposo.getLongitude()
                );
                if(triposo.getLista().equals("VISITADO")) {
                    Marker marker = googleMap.addMarker(new MarkerOptions()
                            .title(triposo.getName())
                            .snippet(triposo.getLocation_id())

                            .icon(BitmapDescriptorFactory.defaultMarker
                                    (BitmapDescriptorFactory.HUE_GREEN))
                            .position(aux)
                    );
                    marker.setTag(triposo);
                    googleMap.setInfoWindowAdapter(customInfoWindow);
                }
                else {
                Marker marker = googleMap.addMarker(new MarkerOptions()
                        .title(triposo.getName())
                        .snippet(triposo.getLocation_id())
                        .icon(BitmapDescriptorFactory.defaultMarker
                                (BitmapDescriptorFactory.HUE_YELLOW))
                        .position(aux)
                );
                marker.setTag(triposo);
                googleMap.setInfoWindowAdapter(customInfoWindow);
                }
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {
            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        });
        }

    @Override
    public void onLocationChanged(@NonNull Location location) {
        saveLocation(location);
    }

    private void saveLocation(Location location) {
        reference.setValue(location);
    }
}
