package com.example.misviajes;

public class Camara {

    private String imageurl;

    public Camara(String imageurl) {
        this.imageurl = imageurl;
    }

    public Camara( ) {
    }


    public String getImageurl() {
        return imageurl;
    }

    public void setImageurl(String imageurl) {
        this.imageurl = imageurl;
    }
}
