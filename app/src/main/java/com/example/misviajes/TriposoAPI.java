package com.example.misviajes;

import android.net.Uri;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.IOException;
import java.util.ArrayList;

public class TriposoAPI {

    private ArrayList<Triposo> locations;
    private Triposo triposo;

    String BASE_URL = "https://www.triposo.com/api/20201111";  // URL PARA TODAS LAS LLAMADAS
    String ACCOUNT_ID = "589NV5PN";
    String TOKEN = "aatoubnfxkczat93xkmuqyfdhuk4xk3f"; // CLAVE
    //String location = "Italy";

    ArrayList<Triposo> getPOILocation(String ciudad) {
        Uri builtUri = Uri.parse(BASE_URL)
                .buildUpon()
                .appendPath("poi.json")
                .appendQueryParameter("location_id", ciudad.replace(" ", "_"))
                .appendQueryParameter("account", ACCOUNT_ID)
                .appendQueryParameter("token", TOKEN)
                .build();
        String url = builtUri.toString()
                .replace("%26", "")
                .replace("%3F", "?")
                .replace("%3D", "=");
        //System.out.println("URL: "+url);
        //System.out.println("prova replace:"+ ciudad);
        return doCall(url);
    }

    ArrayList<Triposo> doCall(String url) {
        try {
            String JsonResponse = HttpUtils.get(url);
            return processJson(JsonResponse);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private ArrayList<Triposo> processJson(String jsonResponse) {
        locations=new ArrayList<>();
        try {

            //LLAMADA AL JSON Y CONTO LOS RESULTADOS TOTALES
            JSONObject joPOILocation = (JSONObject) new JSONTokener(jsonResponse).nextValue();
            JSONArray joTotResults = joPOILocation.getJSONArray("results");

            System.out.println(joTotResults);

            for (int i = 0; i <joTotResults.length(); i++) {
                JSONObject joResults = joPOILocation.getJSONArray("results").getJSONObject(i);
                JSONObject joImages =  joResults.getJSONArray("images").getJSONObject(0);
                JSONObject joCoordinates = joResults.getJSONObject("coordinates");

                triposo = new Triposo();

                triposo.setName(joResults.getString("name"));
                triposo.setLocation_id(joResults.getString("location_id"));
                triposo.setSnippet(joResults.getString("snippet"));
                triposo.setImage(joImages.getString("source_url"));
                triposo.setLatitude(joCoordinates.getDouble("latitude"));
                triposo.setLongitude(joCoordinates.getDouble("longitude"));

                locations.add(triposo);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        System.out.println("TOTAL : "+locations);
        return locations;
    }

    /*private boolean controlarLocation(String location) {
        for (int i = 0; i <locations.size() ; i++) {
            if (locations.get(i).getLocation_id().equals(location)){
                return true;
            }
        }
        return false;
    }*/


}
